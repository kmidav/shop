<?php


namespace App\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ContactUsFormModel
{
    /**
     * @Assert\NotBlank(message="Введите email!")
     * @Assert\Email(message="Введите корректный email!")
     */
    public $email;

    /**
     * @Assert\NotBlank(message="Введите имя!")
     */
    public $name;

    public $phoneNumber;

    public $company;

    /**
     * @Assert\NotBlank(message="Вы что то хотели нам написать, ведь правда?")
     */
    public $message;
}