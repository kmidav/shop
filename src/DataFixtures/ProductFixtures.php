<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    private static $images1 = [
        'product-1-0.jpg',
        'product-1-1.jpg',
    ];

    private static $images2 = [
      'product-2-0.jpg',
      'product-2-1.jpg',
    ];

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $product1 = new Product();
        $product1->setName('ДEМПФЕР РЕЗИНОВЫЙ')
            ->setDescription('Демпфер резиновый (амортизатор) 
            крепления стойки дисковой бороны. Изготовлен из силовой резины. Размер 30х190мм.')
            ->setImages(self::$images1)
            ->setNumber(1)
            ->setPrice('185');
        $manager->persist($product1);

        $product1 = new Product();
        $product1->setName('ВКЛАДЫШ У 36-16-14-2')
            ->setDescription('Вкладыш (палец резиновый) применяется в составе муфты соединительной для дизельных двигателей Д6, Д12 и генераторов ГСФ. Размер пальца 40x60мм.')
            ->setImages(self::$images2)
            ->setNumber(2)
            ->setPrice('97');
        $manager->persist($product1);

        $manager->flush();
    }
}
