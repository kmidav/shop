<?php

namespace App\Controller;

use App\Form\ContactUsFormType;
use App\Form\Model\ContactUsFormModel;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    /**
     * @Route("/", name="app_about")
     */
    public function about()
    {
        return $this->render('about/about.html.twig');
    }

    /**
     * @Route("/contacts", name="app_contacts")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function contacts(Request $request, MailerInterface $mailer)
    {
        $form = $this->createForm(ContactUsFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var ContactUsFormModel $latterModel */
            $letterModel = $form->getData();

            $email = (new TemplatedEmail())
                ->from($letterModel->email)
                ->to('gradient.barnaul@gmail.com')
                ->subject($letterModel->name)
                ->htmlTemplate('emails/contact.html.twig')
                ->context([
                    'name' => $letterModel->name,
                    'company' => $letterModel->company,
                    'phoneNumber' => $letterModel->phoneNumber,
                    'message' => $letterModel->message,
                    'from_email' => $letterModel->email,
                ]);

            $mailer->send($email);
            $this->addFlash('success', 'Сообщение отправлено!');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Сообщение не отправлено! Введите корректные данные!');
        }

        return $this->render("about/contacts.html.twig", [
            'contactForm' => $form->createView()
        ]);
    }
}
