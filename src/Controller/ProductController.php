<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/services", name="app_services")
     */
    public function showServices()
    {
        return $this->render('product/services.html.twig');
    }

    /**
     * @Route("/products", name="app_products")
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function showAllProducts(ProductRepository $productRepository)
    {
        $products = $productRepository->findAll();

        return $this->render('product/products.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("product/{slug}", name="app_product_show")
     * @param $slug
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function showProduct($slug, ProductRepository $productRepository)
    {
        $product = $productRepository->findOneBy(['slug' => $slug]);

        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }
}
